# 🛸自用代码提交格式
## Web Bookmark
[GitHub 支持的 Emoji 列表](https://gist.github.com/rxaviers/7360908)
[Emoji 搜索](https://emojipedia.org/)

## Commit Title

### :art:`:art:`
改进代码的结构/格式。

### :zap:`:zap:`
提高性能。

### :fire:`:fire:`
移除代码/文件。

### :bug:`:bug:`
修复bug。

### :ambulance:`:ambulance:`
重要修复。

### :sparkles:`:sparkles:`
增加新功能。

### :pencil:`:pencil:`
编写文档。

### :rocket:`:rocket:`
部署项目。

### :lipstick:`:lipstick:`
更新UI样式。

### :tada:`:tada:`
初始化提交。

### :white_check_mark:`:white_check_mark:`
更新测试例。

### :lock:`:lock:`
修复安全相关issues。